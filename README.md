Laravel Livewire Tests
Barryvdh ide helper is included to help with PHPDocs.

Step 1: composer create-project --prefer-dist laravel/laravel Laravel8CRUD
Step 2: composer require laravel/jetstream
Step 3: php artisan jetstream:install livewire
Step 4: npm install && npm run dev
Step 5: php artisan migrate

Two livewire files after running php artisan make:livewire modelname:-
1. app/Http/Livewire/Model.php
2. resources/views/livewire/model.blade.php